
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
//#include <sys/times.h>
//#include <sys/auxv.h>

//#define _BSD_SOURCE

#define E 0.000001
#define N 50 * 8
#define ROOT_RANK 0

void mult_matrix(int x, int m, int y, float *matrix_a,
                 float *matrix_b, float *matrix_res) {
  int i, j, k;
  for (i = 0; i < x; i++) {
    for (j = 0; j < y; j++) {
      matrix_res[i*y + j] = 0;
      for (k = 0; k < m; k++) {
        matrix_res[i*y + j] += matrix_a[i*m + k] * matrix_b[k*y + j];

      }

    }
  }
}

float scalar_mult_vector(int size, float *vector_a, float *vector_b, int start, int end) {
  float result = 0;
  int i;
  for (i = start; i < end; i++) {
    result += vector_a[i] * vector_b[i];
  }
  return result;
}

float abs_vector(int size, float *vector, int start, int end) {
  float abs = 0;
  int i;
  for (i = start; i < end; i++)
    abs += vector[i] * vector[i];
  abs = sqrt(abs);
  return abs;
}

void conjugate_gradient_method(int size, float *thread_matrix,  float *result_vector, float *x) {
  //MPI_Init(&argc, &argv);
  int thread_max, thread_rank;
  MPI_Comm_size(MPI_COMM_WORLD,&thread_max);
  MPI_Comm_rank(MPI_COMM_WORLD,&thread_rank);
  //int index_start = floor((float)thread_rank * (float)size / (float)thread_max);
  //int index_end = floor((float)(thread_rank + 1) * (float)size / (float)thread_max);
  printf("thread_rank 1: %d\n", thread_rank);
  int thread_size = size / thread_max;


  float *r = (float *)malloc(sizeof(float)*size);
  float *r_new= (float *)malloc(sizeof(float)*size);
  float *z = (float *)malloc(sizeof(float)*size);
  float a, b;
  float *temp= (float *)malloc(sizeof(float)*size);
  float *thread_temp = (float *) malloc(sizeof(float) * thread_size);
  float *thread_x = (float *) malloc(sizeof(float) * thread_size);
  float *thread_result_vector = (float *) malloc(sizeof(float) * thread_size);
  float *thread_r = (float *) malloc(sizeof(float) * thread_size);
  float *thread_r_new = (float *) malloc(sizeof(float) * thread_size);
  float *thread_z = (float *) malloc(sizeof(float) * thread_size);
  int i;
  int index_start = thread_size * thread_rank;
  int index_end = thread_size * (thread_rank + 1);
  printf("thread_rank 2: %d\n", thread_rank);
  for (i = 0; i < thread_size; i++)
    thread_x[i] = 0;  // X
  MPI_Allgather(thread_x, thread_size, MPI_FLOAT,
         x, thread_size, MPI_FLOAT, MPI_COMM_WORLD);

  mult_matrix(thread_size, size, 1, thread_matrix, x, thread_temp); // X
        //MPI_Barrier(MPI_COMM_WORLD);

        MPI_Allgather(thread_temp, thread_size, MPI_FLOAT,
          temp, thread_size, MPI_FLOAT, MPI_COMM_WORLD);
        //MPI_Bcast(temp, size, MPI_FLOAT, ROOT_RANK, MPI_COMM_WORLD);
        //MPI_Barrier(MPI_COMM_WORLD);
  for (i = 0; i < thread_size; i++) {
    thread_r[i] = thread_result_vector[i] - thread_temp[i]; // B
    thread_z[i] = thread_r[i];
  }

    MPI_Allgather(thread_r, thread_size, MPI_FLOAT,
       r, thread_size, MPI_FLOAT, MPI_COMM_WORLD);
    MPI_Allgather(thread_z, thread_size, MPI_FLOAT,
       z, thread_size, MPI_FLOAT, MPI_COMM_WORLD);
  printf("thread_rank 3: %d\n", thread_rank);
    if (index_start != index_end)
      do {
  printf("thread_rank 4: %d\n", thread_rank);
        mult_matrix(thread_size, size, 1, thread_matrix, z, thread_temp);
        //MPI_Barrier(MPI_COMM_WORLD);
        MPI_Allgather(thread_temp, thread_size, MPI_FLOAT,
          temp, thread_size, MPI_FLOAT, MPI_COMM_WORLD);
  printf("thread_rank 5: %d\n", thread_rank);
        //MPI_Bcast(temp, size, MPI_FLOAT, ROOT_RANK, MPI_COMM_WORLD);
        //MPI_Barrier(MPI_COMM_WORLD);
        a = scalar_mult_vector(size, r, r, 0, size) / scalar_mult_vector(size, temp, z, 0, size);
        for (i = 0; i < thread_size; i++) {
          thread_x[i] = thread_x[i] + a * thread_z[i]; // X
          thread_r_new[i] = thread_r[i] - a * thread_temp[i];
        }
    MPI_Allgather(thread_x, thread_size, MPI_FLOAT,
       x, thread_size, MPI_FLOAT, MPI_COMM_WORLD);
    MPI_Allgather(thread_r_new, thread_size, MPI_FLOAT,
       r_new, thread_size, MPI_FLOAT, MPI_COMM_WORLD);
  printf("thread_rank 6: %d\n", thread_rank);

        //MPI_Barrier(MPI_COMM_WORLD);
        b = scalar_mult_vector(size, r_new, r_new, 0, size) /
            scalar_mult_vector(size, r, r, 0, size);
        //MPI_Barrier(MPI_COMM_WORLD);
        for (i = 0; i < size; i++) {
         thread_z[i] = thread_r_new[i] + b * thread_z[i];
         thread_r[i] = thread_r_new[i];
        }
  printf("%d: %f %f\n", thread_rank, thread_z[0], z[0]);
  printf("thread_rank 6.5: %d\n", thread_rank);
    MPI_Allgather(thread_z, thread_size, MPI_FLOAT,
       z, thread_size, MPI_FLOAT, MPI_COMM_WORLD);
  printf("thread_rank 7: %d\n", thread_rank);
    MPI_Allgather(thread_r, thread_size, MPI_FLOAT,
       r, thread_size, MPI_FLOAT, MPI_COMM_WORLD);
  printf("thread_rank 8: %d\n", thread_rank);
        //MPI_Barrier(MPI_COMM_WORLD);
      } while (abs_vector(size, r, 0, size) / abs_vector(size, result_vector, 0, size) > E); // B
  printf("thread_rank 9: %d\n", thread_rank);
  free(r);
  free(r_new);
  free(z);
  free(temp);
  free(thread_temp);
  free(thread_r);
  free(thread_r_new);
  free(thread_z);
  free(thread_result_vector);
  free(thread_x);
  printf("thread_rank 10: %d\n", thread_rank);
  //free(thread_matrix);
}

int main(int argc, char*argv[]) {
  //int thread_num = atoi(argc[1]);
  MPI_Init(&argc, &argv);
  float *matrix_coefficient;
  float *result_vector = (float *)malloc(sizeof(float)*N);
  float *variable_vector = (float *)malloc(sizeof(float)*N);
  float u[N];
  int i, j;
  int thread_max, thread_rank;
  MPI_Comm_size(MPI_COMM_WORLD,&thread_max);
  MPI_Comm_rank(MPI_COMM_WORLD,&thread_rank);
  int thread_size = N / thread_max;
  if (thread_rank == ROOT_RANK){
  matrix_coefficient = (float *)malloc(sizeof(float)*N*N);
  for (i = 0; i < N; i++) {
    u[i] = sin(2*3.14159/(float)N);
    for (j = 0; j < N; j++) {
      if (i == j)
        matrix_coefficient[i*N + j] = 2;
      else
        matrix_coefficient[i*N + j] = 1;
    }
  }
  mult_matrix(N, N, 1, matrix_coefficient, u, result_vector);
}
  float *thread_matrix = (float *) malloc(sizeof(float) * thread_size * N);
    MPI_Scatter (matrix_coefficient, thread_size * N, MPI_FLOAT,
      thread_matrix, thread_size * N, MPI_FLOAT, ROOT_RANK, MPI_COMM_WORLD);
  if (thread_rank == ROOT_RANK)
    free(matrix_coefficient);
  //for (i = 0; i < N; i++)
  //  result_vector[i] = (float)N + 1.0;

  //struct timeval start, end;
  //long clocks;
  //gettimeofday(&start, NULL);
  double start = MPI_Wtime();

  conjugate_gradient_method(N, thread_matrix, result_vector, variable_vector);
//MPI_Finalize();
  double end = MPI_Wtime();
  //gettimeofday(&end, NULL);
  //clocks = (end.tv_sec - start.tv_sec) * 1000000 + end.tv_usec - start.tv_usec;


    if (thread_rank == ROOT_RANK){
  printf("Time taken on %f seconds\n", end - start);
    //for (i = 0; i < N; ++i)
    //  printf("%f ", variable_vector[i]);
    }
  printf("\n");
  //free(matrix_coefficient);
  free(result_vector);
  free(variable_vector);
  free(thread_matrix);
MPI_Finalize();
}
